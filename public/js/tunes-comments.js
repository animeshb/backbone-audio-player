(function($) {
	
	window.Album = Backbone.Model.extend({

		isFirstTrack: function(index) {
			return index == 0;
		},

		isLastTrack: function(index) {
			return index >= this.get('tracks').length -1; 
		},

		trackUrlAtIndex: function(index) {
			if(this.get('tracks').length >= index) {
				return this.get('tracks')[index].url;
			}
			return null;
		}

	});

	window.Albums = Backbone.Collection.extend({
		model: Album,
		// the url is so we can call fetch, 
		// it will make a call to the server
		// and asynchronously populate the collection.
		url: '/albums'
	});

	window.Playlist = Albums.extend({

		isFirstAlbum: function(index) {
			return index == 0
		},

		isLastAlbum: function(index) {
			return index == (this.models.length - 1);
		}
	});

	window.Player = Backbone.Model.extend({
		defaults: {
			'currentAlbumIndex': 0,
			'currentTrackIndex': 0,
			'state': 'stop'
		},

		initialize: function() {
			this.playlist = new Playlist();
		},

		play: function() {
			this.set({'state': 'play'});
		},

		pause: function() {
			this.set({'state': 'pause'});
		},

		isPlaying: function() {
			return this.get('state') == 'play';
		},

		isStopped: function() {
			return (!this.isPlaying());
		},

		currentAlbum: function() {
			return this.playlist.at(this.get('currentAlbumIndex'));
		},

		currentTrackUrl: function() {
			var album = this.currentAlbum();
			return album.trackUrlAtIndex(this.get('currentTrackIndex'));
		},

		nextTrack: function() {
			var currentTrackIndex = this.get('currentTrackIndex'),
					currentAlbumIndex = this.get('currentAlbumIndex');
			// first check if we are at the last track
			if (this.currentAlbum().isLastTrack(currentTrackIndex)) {
				// okay, so we are at the last track
				// are there any other albums or should
				// we just go to the top of this album
				if (this.playlist.isLastAlbum(currentAlbumIndex)) {
					// okay so it is the last album
					// then just go to the first track 
					// of the first album
					this.set({'currentAlbumIndex': 0});
					this.set({'currentTrackIndex': 0});
				} else {
					// okay then it is not the last album
					// in the playlist so move to the next 
					// album and play the first song
					this.set({'currentAlbumIndex': currentAlbumIndex + 1});
					this.set({'currentTrackIndex': 0});
				}
			} else {
				// there is another track so just increment
				this.set({'currentTrackIndex': currentTrackIndex + 1});
			}
			this.logCurrentAlbumAndTrack();
		},

		prevTrack: function() {
			var currentTrackIndex = this.get('currentTrackIndex'),
					currentAlbumIndex = this.get('currentAlbumIndex');
			// first check if we are at the first track
			if (this.currentAlbum().isFirstTrack(currentTrackIndex)) {
				// okay so we are at the first track
				// now check if we are at the first album 
				// or not
				if (this.playlist.isFirstAlbum(currentAlbumIndex)) {
					// okay so this is the first album
					// so we need to go to the very last track
					// of the last album
					this.set({'currentAlbumIndex': this.playlist.models.length - 1});
					this.set({'currentTrackIndex': this.currentAlbum().get('tracks').length - 1});
				} else {
					// okay so there is an album 
					// that comes before this one
					// just decrement both
					this.set({'currentAlbumIndex': currentAlbumIndex - 1});
					this.set({'currentTrackIndex': this.currentAlbum().get('tracks').length - 1});
				}
			} else {
				// there is a track before so just decrement
				this.set({'currentTrackIndex': currentTrackIndex - 1});
			}
			this.logCurrentAlbumAndTrack();
		},

		logCurrentAlbumAndTrack: function() {
			console.log('Player ' + this.get('currentAlbumIndex') + ':' + this.get('currentTrackIndex'), this);
		}
	});

	window.library = new Albums();
	window.player = new Player();

	window.AlbumView = Backbone.View.extend({
		tagName: 'li',
		className: 'album',

		initialize: function() {
			// so we don't have to say this.render
			
			this.model.on('change', this.render);
			this.template = _.template($('#album-template').html());
		},

		render: function() {
			// combine template and data
			// use built in template from above
			var renderedContent = this.template(this.model.toJSON());
			// append to view element.
			$(this.el).html(renderedContent);
			return this;
		}
	});

	window.LibraryAlbumView = AlbumView.extend({
		events: {
			'click .queue.add': 'select',
		},

		select: function() {
			this.collection.trigger('select', this.model);
		}
	});

	window.PlaylistAlbumView = AlbumView.extend({
		events: {
			'click .queue.remove': 'removeFromPlaylist'
		},

		initialize: function() {
			_.bindAll(this, 'render', 
											'remove',
											'updateState',
											'updateTrack');
			this.player = this.options.player;
			this.player.on('change:state', this.updateState);
			this.player.on('change:currentTrackIndex', this.updateTrack);
			this.model.on('remove', this.remove);
			this.template = _.template($('#album-template').html());
		},

		removeFromPlaylist: function() {
			// it's easier to just handle the remove
			// from within the view
			// we still need to remove from the dom
			this.options.playlist.remove(this.model);
		},

		updateState: function() {
			var isAlbumCurrent = (this.player.currentAlbum() === this.model);
			$(this.el).toggleClass('current', isAlbumCurrent);
		},

		updateTrack: function() {
			var isAlbumCurrent = (this.player.currentAlbum() === this.model);
			if (isAlbumCurrent) {
				var currentTrackIndex = this.player.get('currentTrackIndex');
				this.$('li').each(function(index, el) {
					$(el).toggleClass('current', index == currentTrackIndex);
				});
			}
			this.updateState();
		}
	});

	window.PlaylistView = Backbone.View.extend({
		tagName: 'section',
		className: 'playlist',

		events: {
			'click .play': 'play',
			'click .pause': 'pause',
			'click .next': 'nextTrack',
			'click .prev': 'prevTrack'
		},

		initialize: function() {
			_.bindAll(this, 'render', 'queueAlbum', 'renderAlbum', 'updateState', 'updateTrack');
			this.template = _.template($('#playlist-template').html());
			this.collection.on('reset', this.render);
			this.collection.on('add', this.renderAlbum);

			// any arguments passed into the view
			// upon creation can be accessed in the
			// options object
			this.player = this.options.player;
			this.player.on('change:state', this.updateState);
			this.player.on('change:currentTrackIndex', this.updateTrack);
			this.createAudio();

			this.library = this.options.library;
			this.library.on('select', this.queueAlbum);
		},

		createAudio: function() {
			this.audio = new Audio();
		},

		render: function() {
			$(this.el).html(this.template(this.player.toJSON()));

			// this.$ searches within the view rather
			// then searching the entire dom
			this.$('button.play').toggle(this.player.isStopped());
			this.$('button.pause').toggle(this.player.isPlaying());
			
			return this;
		},

		renderAlbum: function(album) {
			var view = new PlaylistAlbumView({
				model: album,
				player: this.player,
				playlist: this.collection
			});
			this.$('ul').append(view.render().el);
		},

		queueAlbum: function(album) {
			// remember, when a model is added
			// to a collection, backbone triggers
			// an add event
			this.collection.add(album);
		},

		updateState: function() {
			this.updateTrack();
			this.$('button.play').toggle(this.player.isStopped());
			this.$('button.pause').toggle(this.player.isPlaying());
		},

		updateTrack: function() {
			this.audio.src = this.player.currentTrackUrl();
			if (this.player.get('state') == 'play') {
				this.audio.play();
			} else {
				this.audio.play();
			}
		},

		play: function() {
			this.audio.play();
		},

		pause: function() {
			this.player.pause();
		},

		nextTrack: function() {
			this.player.nextTrack();
		},

		prevTrack: function() {
			this.player.prevTrack();
		}

	});

	window.LibraryView = Backbone.View.extend({

		tagName: 'section',
		className: 'library',

		initialize: function() {
			_.bindAll(this, 'render');
			this.template = _.template($('#library-template').html());
			this.collection.on('reset', this.render);
		},

		render: function() {
			// this.$(pattern) = search within element
			// find ul so we can insert albums into it
			var $albums,
					collection = this.collection;
			// pass the blank template in
			$(this.el).html(this.template({}));
			$albums = this.$('.albums');
			collection.each(function(album) {
				var view = new LibraryAlbumView({
					model: album,
					collection: collection
				});
				// now we need to render the view 
				// we are creating
				$albums.append(view.render().el);
			}); 
			
			return this;
		}
	});

	window.BackboneTunes = Backbone.Router.extend({
		routes: {
			'': 'home'
		},

		initialize: function() {
			// dont use global vars in view
			// pass them in
			this.playlistView = new PlaylistView({
				collection: window.player.playlist,
				player: window.player,
				library: window.library
			});
			// should instantiate applications root level view
			this.libraryView = new LibraryView({ 
				collection: window.library 
			});
		},

		home: function() {
			// grab the root element that you
			// are going to be manipulating
			var $container = $('#container');
			// ensure that it is empty
			$container.empty();
			// render the playlist view on startup
			$container.append(this.playlistView.render().el);
			// append the root view
			$container.append(this.libraryView.render().el);
		}
	});

	// now we need to instantiate the router
	$(function() {
		// iffe
		// by default #
		window.App = new BackboneTunes();
		Backbone.history.start();
	});

})(jQuery);
